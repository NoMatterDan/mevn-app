import { createApp } from 'vue'
import firebase from './plugins/firebase'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.min.css'

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

const app = createApp(App)

app.use(firebase, {})

app.mount('#app')
