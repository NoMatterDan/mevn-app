import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyA18FYC0r4WEdK2snnK100z_Yw-gBTcx1A",
  authDomain: "mevn-b9605.firebaseapp.com",
  projectId: "mevn-b9605",
  storageBucket: "mevn-b9605.appspot.com",
  messagingSenderId: "240770090200",
  appId: "1:240770090200:web:2518a0b9fe6a55fbd8c3ad"
};

export default { 
  install: (app, options) => {
    app.config.globalProperties.$firebase = initializeApp(firebaseConfig)

    app.provide('firebase', options)
  }
}